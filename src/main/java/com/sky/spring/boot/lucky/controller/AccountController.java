package com.sky.spring.boot.lucky.controller;

import com.sky.spring.boot.lucky.pojo.AccountUser;
import com.sky.spring.boot.lucky.service.AccountUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author 0nlkk
 * @description 功能描述
 * @create 2023/12/18 19:51
 */
@RestController
@RequestMapping("account")
public class AccountController {
    @Autowired
    private AccountUserService accountUserService;

    @PostMapping("/list")
    public ResponseEntity<List<AccountUser>> getAccountList() {
        return ResponseEntity.ok(accountUserService.list());
    }
}
