package com.sky.spring.boot.lucky.mapper;
import org.apache.ibatis.annotations.Param;
import java.util.Collection;

import com.sky.spring.boot.lucky.pojo.AccountUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 0nlkk
* @description 针对表【account_user(用户)】的数据库操作Mapper
* @createDate 2023-12-12 22:33:52
* @Entity com.sky.springbootchat.pojo.AccountUser
*/
public interface AccountUserMapper extends BaseMapper<AccountUser> {
    int insertBatch(@Param("accountUserCollection") Collection<AccountUser> accountUserCollection);
}




