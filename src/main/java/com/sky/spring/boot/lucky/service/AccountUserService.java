package com.sky.spring.boot.lucky.service;

import com.sky.spring.boot.lucky.pojo.AccountUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 0nlkk
* @description 针对表【account_user(用户)】的数据库操作Service
* @createDate 2023-12-12 22:33:52
*/
public interface AccountUserService extends IService<AccountUser> {

}
