package com.sky.spring.boot.lucky.log;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.p6spy.engine.spy.appender.MessageFormattingStrategy;

/**
 * @author 0nlkk
 * @description 自定义日志格式
 * @create 2023/12/18 20:20
 */
public class CustomP6SpyLogger implements MessageFormattingStrategy {

    /**
     * Sql日志格式化
     *
     * @param connectionId: 连接ID
     * @param now:          当前时间
     * @param elapsed:      花费时间
     * @param category:     类别
     * @param prepared:     预编译SQL
     * @param sql:          最终执行的SQL
     * @param url:          数据库连接地址
     * @return 格式化日志结果
     */
    @Override
    public String formatMessage(int connectionId, String now, long elapsed, String category, String prepared, String sql, String url) {
        String ANSI_RESET = "\u001B[0m";
        String ANSI_GREEN = "\u001B[32m";
        String ANSI_BLUE = "\u001B[34m";
        return StringUtils.isNotBlank(sql) ?
                now + " sql耗时：" + ANSI_GREEN + elapsed + " ms \n" + ANSI_RESET + "执行 SQL：" + ANSI_BLUE + sql.replaceAll("[\\s]+", " ") + "\n" + ANSI_RESET : "";
    }
}
