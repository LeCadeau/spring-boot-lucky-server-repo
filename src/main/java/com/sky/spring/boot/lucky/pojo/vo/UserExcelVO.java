package com.sky.spring.boot.lucky.pojo.vo;

import com.sky.spring.boot.lucky.pojo.UploadData;
import lombok.Data;

import java.util.List;

/**
 * @author 0nlkk
 * @description 功能描述
 * @create 2023/12/14 21:08
 */
@Data
public class UserExcelVO {

    /**
     * 检查通过列表
     */
//    private List<UploadData> checkPassed;


    /**
     * 检查未通过列表
     */
    private List<UploadData> checkFailed;

    /**
     * 导入总数量
     */
    private int totalCount;


    /**
     * 检查通过数量
     */
    private int checkPassCount;

    /**
     * 检查未通过数量
     */
    private int checkFailCount;

    /**
     * 导入成功数量
     */
    private int actualSuccessCount;
}
