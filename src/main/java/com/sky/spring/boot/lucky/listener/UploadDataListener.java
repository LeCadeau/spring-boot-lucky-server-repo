package com.sky.spring.boot.lucky.listener;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.read.listener.ReadListener;
import com.alibaba.excel.util.ListUtils;
import com.alibaba.fastjson2.JSON;
import com.sky.spring.boot.lucky.dao.UploadDAO;
import com.sky.spring.boot.lucky.pojo.UploadData;
import com.sky.spring.boot.lucky.utils.ValidationUtils;
import jakarta.validation.ConstraintViolation;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * @author 0nlkk
 * @description 模板的读取类
 * @create 2023/12/12 21:33
 */
// 有个很重要的点 DemoDataListener 不能被spring管理，要每次读取excel都要new,然后里面用到spring可以构造方法传进去
@Slf4j
public class UploadDataListener implements ReadListener<UploadData> {
    /**
     * 每隔5条存储数据库，实际使用中可以100条，然后清理list ，方便内存回收
     */
    private static final int BATCH_COUNT = 5;

    @Getter
    private int successCount;

    private List<UploadData> cachedDataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
    /**
     * 假设这个是一个DAO，当然有业务逻辑这个也可以是一个service。当然如果不用存储这个对象没用。
     */
    private UploadDAO uploadDAO;

    @Getter
    List<UploadData> fail = new ArrayList<>();

    public UploadDataListener() {
        // 这里是demo，所以随便new一个。实际使用如果到了spring,请使用下面的有参构造函数
        uploadDAO = new UploadDAO();
    }

    /**
     * 如果使用了spring,请使用这个构造方法。每次创建Listener的时候需要把spring管理的类传进来
     *
     * @param uploadDAO
     */
    public UploadDataListener(UploadDAO uploadDAO) {
        this.uploadDAO = uploadDAO;
    }

    /**
     * 这个每一条数据解析都会来调用
     *
     * @param data    one row value. It is same as {@link AnalysisContext#readRowHolder()}
     * @param context
     */
    @Override
    public void invoke(UploadData data, AnalysisContext context) {
        log.info("解析到一条数据:{}", JSON.toJSONString(data));

        Set<ConstraintViolation<UploadData>> violations = ValidationUtils.getValidator().validate(data);
        if (!violations.isEmpty()) {
            ArrayList<String> list = ListUtils.newArrayList();
            violations.forEach(violation -> log.warn("验证失败的属性名称：{}，失败的原因：{}", violation.getPropertyPath().toString(), violation.getMessage()));
            violations.forEach(violation -> {
                try {
                    list.add("当前行号：" + (context.readRowHolder().getRowIndex() + 1)
                            + "，校验失败的属性名称：" + Arrays.toString(UploadData.class.getDeclaredField(violation.getPropertyPath().toString()).getAnnotation(ExcelProperty.class).value())
                            + "，校验失败的属性路径：" + violation.getPropertyPath()
                            + "，校验失败的属性值：" + violation.getInvalidValue()
                            + "，校验失败的错误信息：" + violation.getMessage());
                } catch (NoSuchFieldException e) {
                    throw new RuntimeException(e);
                }
            });
            data.setFailReasons(list);
            fail.add(data);
        } else {
//            for (UploadData uploadData : cachedDataList) {
//                if (uploadData.getUsername().equalsIgnoreCase(data.getUsername())) return;
//            }

            if (cachedDataList.stream().anyMatch(uploadData -> uploadData.getUsername().equalsIgnoreCase(data.getUsername())))
                return;

            log.warn("验证成功的数据：{}", data);
            cachedDataList.add(data);
            // 达到BATCH_COUNT了，需要去存储一次数据库，防止数据几万条数据在内存，容易OOM
            if (cachedDataList.size() >= BATCH_COUNT) {
                saveData();
                // 存储完成清理 list
                cachedDataList.clear();
            }
        }
    }

    /**
     * 所有数据解析完成了 都会来调用
     *
     * @param context
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        // 这里也要保存数据，确保最后遗留的数据也存储到数据库
        saveData();
        log.info("所有数据解析完成！总数量为:{}", successCount);
    }

    /**
     * 加上存储数据库
     */
    private void saveData() {
        if (!cachedDataList.isEmpty()) {
            log.info("{}条数据，开始存储数据库！", cachedDataList.size());
            int count = uploadDAO.save(cachedDataList);
            System.out.println("count = " + count);
            successCount += count;
            log.info("存储数据库成功！");
        }

    }
}
