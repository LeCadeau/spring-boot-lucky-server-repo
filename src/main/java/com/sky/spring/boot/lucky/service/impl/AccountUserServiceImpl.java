package com.sky.spring.boot.lucky.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sky.spring.boot.lucky.pojo.AccountUser;
import com.sky.spring.boot.lucky.service.AccountUserService;
import com.sky.spring.boot.lucky.mapper.AccountUserMapper;
import org.springframework.stereotype.Service;

/**
* @author 0nlkk
* @description 针对表【account_user(用户)】的数据库操作Service实现
* @createDate 2023-12-12 22:33:52
*/
@Service
public class AccountUserServiceImpl extends ServiceImpl<AccountUserMapper, AccountUser>
    implements AccountUserService{

}




