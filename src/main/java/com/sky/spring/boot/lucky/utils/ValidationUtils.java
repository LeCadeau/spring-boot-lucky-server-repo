package com.sky.spring.boot.lucky.utils;

import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import lombok.Getter;

/**
 * @author 0nlkk
 * @description 功能描述
 * @create 2023/12/15 20:00
 */
public class ValidationUtils {

    @Getter
    static Validator validator;
    static{
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        validator=validatorFactory.getValidator();
    }
}
