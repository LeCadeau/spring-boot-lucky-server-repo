package com.sky.spring.boot.lucky.exception;

import com.sky.spring.boot.lucky.web.ServiceCode;
import lombok.Getter;

/**
 * @author 0nlkk
 * @description 业务异常
 * @create 2023/12/14 23:04
 */
@Getter
public class ServiceException extends RuntimeException {

    private ServiceCode serviceCode;

    /**
     * 创建业务异常对象
     *
     * @param serviceCode 业务状态码
     * @param message     描述文本
     */
    public ServiceException(ServiceCode serviceCode, String message) {
        super(message);
        this.serviceCode = serviceCode;
    }

}