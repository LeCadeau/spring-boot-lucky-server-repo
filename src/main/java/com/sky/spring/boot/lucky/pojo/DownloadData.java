package com.sky.spring.boot.lucky.pojo;

import java.util.Date;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * @author 0nlkk
 * @description 基础数据类
 * @create 2023/12/12 21:32
 */
@Getter
@Setter
@EqualsAndHashCode
public class DownloadData {

    @ExcelProperty("数据ID")
    private Long id;


    @ExcelProperty("用户名")
    private String username;


    @ExcelProperty("密码（密文）")
    private String password;


    @ExcelProperty("头像URL")
    private String avatar;


    @ExcelProperty("手机号码")
    private String phone;


    @ExcelProperty("电子邮箱")
    private String email;


    @ExcelProperty("简介")
    private String description;


    @ExcelProperty("是否启用，1=启用，0=未启用")
    private Integer enable;


    @ExcelProperty("最后登录IP地址（冗余）")
    private String lastLoginIp;


    @ExcelProperty("累计登录次数（冗余）")
    private Integer loginCount;


    @ExcelProperty("最后登录时间（冗余）")
    private Date gmtLastLogin;


    @ExcelProperty("数据创建时间")
    private Date gmtCreate;


    @ExcelProperty("数据最后修改时间")
    private Date gmtModified;
}
