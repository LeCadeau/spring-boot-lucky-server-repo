package com.sky.spring.boot.lucky.dao;

import com.alibaba.excel.util.ListUtils;
import com.sky.spring.boot.lucky.mapper.AccountUserMapper;
import com.sky.spring.boot.lucky.pojo.AccountUser;
import com.sky.spring.boot.lucky.pojo.UploadData;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 0nlkk
 * @description 功能描述
 * @create 2023/12/12 21:30
 */
@Repository
public class UploadDAO {

    @Autowired
    AccountUserMapper userMapper;

    public int save(List<UploadData> list) {
        // 如果是mybatis,尽量别直接调用多次insert,自己写一个mapper里面新增一个方法batchInsert,所有数据一次性插入
        ArrayList<AccountUser> userList = ListUtils.newArrayList();
        for (UploadData uploadData : list) {
            AccountUser user = new AccountUser();
            BeanUtils.copyProperties(uploadData, user);
            userList.add(user);
        }
        return userMapper.insertBatch(userList);
//        for (int i = 0; i < list.size(); i++) {
//            list.get(i).setId(userList.get(i).getId());
//        }
    }
}
