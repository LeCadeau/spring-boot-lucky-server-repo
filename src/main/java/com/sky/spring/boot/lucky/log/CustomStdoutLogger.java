package com.sky.spring.boot.lucky.log;

/**
 * @author 0nlkk
 * @description 自定义日志输出
 * @create 2023/12/18 20:25
 */
public class CustomStdoutLogger extends com.p6spy.engine.spy.appender.StdoutLogger {

    @Override
    public void logText(String text) {
        String ANSI_RESET = "\u001B[0m";
        String ANSI_YELLOW = "\u001B[33m";
        System.out.println(ANSI_YELLOW + "== == == == == == == == == == == == \n" +
                ANSI_RESET + text);
    }
}
