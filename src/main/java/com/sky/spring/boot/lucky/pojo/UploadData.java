package com.sky.spring.boot.lucky.pojo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.*;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author 0nlkk
 * @description 基础数据类
 * @create 2023/12/12 21:31
 */
@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class UploadData implements Serializable {

    //    @NotNull(message = "用户ID不能为空")
//    private Long id;

    @ExcelProperty(value = "用户名")
    @NotNull(message = "用户名不能为空")
    private String username;

    @NotNull(message = "密码不能为空")
//    @Pattern(regexp = "^[a-zA-Z0-9|_]+$", message = "密码必须由字母、数字、下划线组成")
//    @Size(min = 6, max = 12, message = "密码长度必须在6-12字符之间")
    @ExcelProperty(value = "密码（密文）")
    private String password;

    @ExcelProperty(value = "头像URL")
    private String avatar;

    @Pattern(regexp = "^[1][3,4,5,7,8][0-9]{9}$$", message = "手机号不合法")
    @NotBlank(message = "手机号不能为空")
    @ExcelProperty(value = "手机号码")
    private String phone;

    @Email
    @ExcelProperty(value = "电子邮箱")
    private String email;

    private String description;

    private Integer enable;

    private String lastLoginIp;

    private Integer loginCount;

    private Date gmtLastLogin;

    private Date gmtCreate;

    private Date gmtModified;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<String> failReasons;
}